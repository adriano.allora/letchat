# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/core/actions/#custom-actions/


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher


class ActionQuandoAmbientazione(Action):

    def name(self) -> Text:
        return "action_quando_ambientazione"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        titolo_opera = tracker.get_slot('titolo_opera') #recuperare le entity

        if titolo_opera is None:
            dispatcher.utter_message(f'Non ho capito il titolo dell\'opera, me lo puoi ripetere?')
#        elif self.is_on_lista(titolo_opera):
#            dispatcher.utter_message(f'messaggio per gestire opere non in catalogo')
        else:
            dispatcher.utter_message(f'Qui ti darò informazioni sull\'ambientazione di {titolo_opera}') 
        return []
    
class ActionTitoloMiglioree(Action):

    def name(self) -> Text:
        return "action_correzione_titolo"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        titolo_opera = tracker.get_slot('titolo_opera') #recuperare le entity

        if titolo_opera is None:
            dispatcher.utter_message(f'Non ho capito il titolo dell\'opera, me lo puoi ripetere?')
#        elif self.is_on_lista(titolo_opera):
#            dispatcher.utter_message(f'messaggio per gestire opere non in catalogo')
        else:
            dispatcher.utter_message(f'Qui ti darò informazioni su {titolo_opera}') 
        return []


#    def is_on_menu(self, pizza_name :str):
#        return f'{pizza_name.capitalize()}' in menu



#titolo_opera = tracker.get_slot("titolo_opera")

#def titolo_opera(self) {
# return self.from_entity(entity="titolo_opera", not_intent="un'opera qualsiasi")
#  }


#def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
#  """un dizionario per map(pare) gli sltos richiesti per
#	- un'entity estratta dalle interazioni
#	- intent:value pairs
#	- un intero messaggio
#     o un insieme di questi, nei quali il primo match sarà preso
#  """

#  return {
#    "titolo_opera": self.from_entity(entity="titolo_opera", not_intent="un'opera qualsiasi"),
#  }
    

#
#
# class ActionHelloWorld(Action):
#
#     def name(self) -> Text:
#         return "action_hello_world"
#
#     def run(self, dispatcher: CollectingDispatcher,
#             tracker: Tracker,
#             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
#
#         dispatcher.utter_message(text="Hello World!")
#
#         return []
