OBIETTIVO DEL PROGETTO
Voglio scrivere un chatbot che chiacchieri con l'utente di storia della letteratura e di quegli argomenti che nelle antologie vanno sotto il nome di "metodi".

Ho scelto rasa perché mi sembrava interessante: il cuore di Rasa è un motore intelligente che riconosce le intenzioni dell'utente e che applica (credo stupidamente) dei pattern interazionali.

Questo NON è un tutorial, mi serve solo per tracciare l'evoluzione del lavoro.

INSTALLARE IL NECESSARIO
Io mi sono creato un env in anaconda per praticità.
Ci ho messo dentro subito la versione di python che ho letto essere migliore per questo genere di applicazioni:

conda create -n rasa_letchat python=3.6
conda activate let_chat
cd anaconda3/envs/rasa_letchat

Una volta là dentro, mi sono installato rasa e spacy (spacy serve per gestire il corpus di italiano che poi rasa usa per il proprio training):

pip install rasa
pip install spacy

poi ho installato il corpus e l'ho reso chiamabile con al stringa "it" (serve dopo):

python -m spacy download it_core_news_sm
python -m spacy link it_core_news_sm it

GIOCARE CON RASA

1 - Lingua di riferimento

Con il comando:

rasa init --no-prompt

Rasa crea un bot minimale ma con la struttura che deve farlo funzionare.
Finita la procedura, bisogna impostare l'italiano come lingua di riferimento del bot, modificando il file config.yml: "language: it".

2 - domini di azione

Il secondo file da modificare è domain.yml.
Il file dei domini raccoglie tre tipi di elementi:
1) un elenco degli intent che il bot deve prendere in considerazione;
2) un elenco delle azioni del bot (all'inizio saranno solo enunciati);
3) un elenco di frasi che corrispondono agli enunciati.

La struttura del file è quindi (io dove non avevo cose nuove da aggiungere ho mantenuto per pigrizia i nomi degli intents in inglese; non è obbligatorio, solo che poi bisogna modificarli dappertutto):

#elenco degli intent
intents:
  - greet
  - thanks
  - quando_ambientazione

#elenco delle azioni del bot
actions:
- utter_greet
- utter_thanks
- utter_quando_ambientazione

#elenco delle frasi di risposta
templates:
  utter_greet:
  - text: Ciao
  - text: Salve
  utter_quando_ambientazione
  - text:qui ti darò info sulla data dell'ambientazione

3 - Descrizione degli intent

Il file data/nlu.dm contiene le definizioni di tutti gli intent, cioè il loro nome e tutti gli esempi che forniremmo a Rasa per fare il suo training.

Quello che ho fatto è stato aggiungere il nuovo intente e corredarlo di una definizione pratica (l'esempio riportato è reale, la sintassi è markdown):

## intent:quando_ambientazione
- quando è ambientata
- in che epoca è ambientata
- in che anno è ambientata
- in quale anno è ambientata

Una volta che anche il file nlu.dm è steso secondo le necessità, possiamo provare a trainarlo e a vedere se il bot riconosce gli intent:

rasa train nlu
rasa shell nlu

L'output indica al primo posto l'intent che risponde meglio alla frase inputata.

4 - Descrizione delle interazioni

Quello che manca è una descrizione di possibili scambi tra bot e utente umano. Rasa poi li potrà mette insieme e ricomporre sulla base degli intent riconosciuti e disponibili.
Il file in cui scrivo le "stories" (così si chiamano), è data/stories.md

La sintassi in stories.md è:

# nome della storia (non vincolante, ma utile per utenti umani)
## happy path 
* greet 			#intent dell'utente (domanda)
  - utter_greet 		#action del bot (risposta)
* quando_ambientazione
  - utter_quando ambientazione
  - utter_did_that_help 	#possono esserci due action di seguito

Quando anche le stories sono compilate (come tutti gli altri, anche il file delle stories cambierà e crescerà nel tempo) possiamo fare il train completo (nlu + core).

rasa train
rasa shell

5 - interazioni avanzate

A questo punto voglio che il software risponda veramente a delle domande, pescando da una sua enciclopedia senza costringermi a costruire un file nlu infinito (è per questo che ho scelto rasa: gestisce gli intent e la mia idea è che l'intent sia il tipo di informazione che l'utente vuole, per esempio "sapere quando è ambientata un'opera", ma poi la risposta alla domanda deve stare fuori dagli intent).
Per prima cosa ho creato le entities che mi interessavano e gli slot.

Questa è una cosa che si fa in tre file:

In domain.yml
1) creo le entities (con la stessa sintassi delle actions):
entities:
  - titolo_opera
  - anno_opera
2) creo gli slots:
slots:
  titolo_opera:
    type: text
    initial_value: l'opera
  anno_opera:
    type: text
    initial_value: nell'anno del non lo so
3) modifico le responses (per convenzione, cambio il nome da utter ad action):
  utter_quando_ambientazione:
  - text: qui dovrebbe esserci una risposta riguardo a quando è ambientata l'opera {titolo_opera}... appena avremo imparato a gestire gli slot le actions

In nlu.md ho modificato l'intent in modo da includere gli slot appena creati:
## intent:quando_ambientazione
- quando è ambientato [furore](titolo_opera)
- in che epoca è ambientato [furore](titolo_opera)
- in che anno è ambientato [furore](titolo_opera)
- in quale anno è ambientato [furore](titolo_opera)

In stories.md ho ragionato in modo da fare lo stesso (anche qui cambia il nome del response):
## domanda quando ambientata
* greet
  - utter_greet
* quando_ambientazione{"titolo_opera": "furore"}
  - action_quando_ambientazione
  - utter_did_that_help

A questo punto ho eseguito rasa train e rasa shell e ho provato a vedere che effetto dava, ma restituiva sempre l'initial value.
Il punto è che recuperare un valore dallo user input significa veramente fare un'azione, quindi è necessario fare altri passaggi:

A) in endpoints.yml bisogna decommentare le due linee di action_endpoint che definiscono l'istanza server di rasa:

action_endpoint:
  url: "http://localhost:5055/webhook"


creare l'action
fare il training


riferimenti:
https://github.com/fabiodn/rasa_tutorial/blob/master/readme.md
https://rasa.com/docs/rasa/core/slots/#id1


forse altro riferimento utile: https://rasa.com/docs/rasa/core/forms/#custom-slot-mappings



class ActionHelloWorld(Action):  
    def name(self) -> Text:
        return "action_hello_world" #Il nome deve essere uguale a quello definito in domain.yml 
#La funzione run è quella chiamata durante la conversazione
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        entity = tracker.get_slot('entity_name') #funzione per recuperare entity
        dispatcher.utter_message("Hello World!") #risposta del bot
        return []
L'action action_show_menu deve leggere le pizze disponibili (dal file data/pizze.txt) e inoltrare le informazioni all'utente.

L'action action_confirm_pizza legge la pizza ordinata dall'utente dallo slot, controlla se la pizza è presente sul menù e di conseguenza comunica al cliente la presa in carico dell'ordine o meno.
ATTENZIONE: lo slot può essere vuoto in caso NLU non riesca a estrarre correttamente il nome della pizza.

A questo punto i file modificati dovrebbero essere qualcosa di simile a questi: domain, stories, actions .

Proviamo il nostro pizza bot aggiornato!
Come abbiamo detto le actions sono un servizio separato quindi dobbiamo lanciare actions e poi il bot.

rasa run actions
rasa run


6 - ESEGUIRE RASA

cd anaconda3/envs
conda activate rasa_letchat
rasa train
rasa run actions
rasa shell



7 - aggiornare il codice

git add .
git commit -m “spiega qui che cos'hai fatto di nuovo”
git push origin nomedelmiobranchsecen'èuno

```
ripasso git:
git fetch    # legge eventuali modifiche sul server
git stash    # mette da parte eventuali modifiche in corso, per poter fare sync
git checkout master    # si sposta sul branch master
git pull    # sincronizza il tuo master con le modifiche che ci sono online
# git branch -D nome_vecchio_branch    # non obbligatorio, ma elimina definitivamente il tuo vecchio branch
git checkout -b nome_nuovo_branch    # crea un nuovo branch