## happy path
* greet
  - utter_greet
* mood_great
  - utter_happy

## domanda quando ambientata
* quando_ambientazione{"titolo_opera": "furore"}
  - action_quando_ambientazione
  - utter_did_that_help

## correzione titolo
* correzione_titolo{"titolo_opera": "uomini e topi"}
  - action_correzione_titolo
  - utter_autocorrezione


## chiedi scusa
* deny
  - utter_fail

## felicità
* affirm
  - utter_happy

## say goodbye
* goodbye
  - utter_goodbye

## bot challenge
* bot_challenge
  - utter_iamabot
